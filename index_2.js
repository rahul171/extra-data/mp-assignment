const axios = require('axios');
const pgp = require('pg-promise')();

const pgUri = "postgres://fchhltfc:vcyFNRGGWwzAIXdQPiTMXcEXz5nYAYLV@satao.db.elephantsql.com:5432/fchhltfc";

const db = pgp(pgUri);

(async function() {

  await prepareDatabase('schema.sql');
  await storeUsers();
  await storePosts();
  await storeAlbums();
  await storeTodos();
  await storeComments();
  await storePhotos();

  db.$pool.end();
})();

async function storeUsers() {
  let res = await axios.get('https://jsonplaceholder.typicode.com/users');
  let users = res.data;
  await addCompanies(users);
  await addGeoLocations(users);
  await addAddresses(users);
  await addUsers(users);
}

async function storePosts() {
  const res = await axios.get('https://jsonplaceholder.typicode.com/posts');
  await addPosts(res.data);
}

async function storeAlbums() {
  const res = await axios.get('https://jsonplaceholder.typicode.com/albums');
  await addAlbums(res.data);
}

async function storeTodos() {
  const res = await axios.get('https://jsonplaceholder.typicode.com/todos');
  await addTodos(res.data);
}

async function storeComments() {
  const res = await axios.get('https://jsonplaceholder.typicode.com/comments');
  await addComments(res.data);
}

async function storePhotos() {
  const res = await axios.get('https://jsonplaceholder.typicode.com/photos');
  await addPhotos(res.data);
}

async function addCompanies(users) {

  let query = 'INSERT INTO rs_company (name, catchPhrase, bs) VALUES ';

  users.forEach((item) => {
    query += `('${item.company.name}', '${item.company.catchPhrase}', '${item.company.bs}'),`;
  });

  query = removeLastChar(query);
  query = addId(query);

  const res = await db.any(query);

  res.forEach((item, index) => {
    users[index].companyId = item.id;
  });
}

async function addGeoLocations(users) {

  let query = 'INSERT INTO rs_geo (lat, lng) VALUES ';

  users.forEach((item) => {
    query += `('${item.address.geo.lat}', '${item.address.geo.lng}'),`;
  });

  query = removeLastChar(query);
  query = addId(query);

  const res = await db.any(query);

  res.forEach((item, index) => {
    users[index].address.geoId = item.id;
  });
}

async function addAddresses(users) {

  let query = 'INSERT INTO rs_address (street, suite, city, zipcode, geoId) VALUES ';

  users.forEach((item) => {
    query += `('${item.address.street}', '${item.address.suite}', '${item.address.city}', '${item.address.zipcode}', `
      + `'${item.address.geoId}'),`;
  });

  query = removeLastChar(query);
  query = addId(query);

  const res = await db.any(query);

  res.forEach((item, index) => {
    users[index].addressId = item.id;
  });
}

async function addUsers(users) {

  let query = 'INSERT INTO rs_users (id, name, username, email, addressId, phone, website, companyId) VALUES ';

  users.forEach((item) => {
    query += `('${item.id}', '${item.name}', '${item.username}', '${item.email}', '${item.addressId}', `
      + `'${item.phone}', '${item.website}', '${item.companyId}'),`;
  });

  query = removeLastChar(query);

  await db.any(query);
}

async function addPosts(posts) {

  let query = 'INSERT INTO rs_post (id, userId, title, body) VALUES ';

  posts.forEach((item) => {
    query += `('${item.id}', '${item.userId}', '${item.title}', '${item.body}'),`;
  });

  query = removeLastChar(query);

  await db.any(query);
}

async function addAlbums(albums) {

  let query = 'INSERT INTO rs_album (id, userId, title) VALUES ';

  albums.forEach((item) => {
    query += `('${item.id}', '${item.userId}', '${item.title}'),`;
  });

  query = removeLastChar(query);

  await db.any(query);
}

async function addTodos(todos) {

  let query = 'INSERT INTO rs_todo (id, userId, title, completed) VALUES ';

  todos.forEach((item) => {
    query += `('${item.id}', '${item.userId}', '${item.title}', '${item.completed}'),`;
  });

  query = removeLastChar(query);

  await db.any(query);
}

async function addComments(comments) {

  let query = 'INSERT INTO rs_comment (id, postId, name, email, body) VALUES ';

  comments.forEach((item) => {
    query += `('${item.id}', '${item.postId}', '${item.name}', '${item.email}', '${item.body}'),`;
  });

  query = removeLastChar(query);

  await db.any(query);
}

async function addPhotos(photos) {

  let query = 'INSERT INTO rs_photo (id, albumId, title, url, thumbnailUrl) VALUES ';

  photos.forEach((item) => {
    query += `('${item.id}', '${item.albumId}', '${item.title}', '${item.url}', '${item.thumbnailUrl}'),`;
  });

  query = removeLastChar(query);

  await db.any(query);
}

function removeLastChar(s) {
  return s.slice(0, -1);
}

function addId(query) {
  return query + ' RETURNING id';
}

async function prepareDatabase(queryFile) {
  const file = getQueryFile(queryFile);
  await db.any(file);
}

function getQueryFile(path) {
  return new pgp.QueryFile(path, { minify: true });
}
