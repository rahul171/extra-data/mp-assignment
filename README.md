### Files

- Node.js Script: [`index.js`](https://gitlab.com/rahul171/extra-data/mp-assignment/blob/master/index.js)
- Configuration data: [`config.json`](https://gitlab.com/rahul171/extra-data/mp-assignment/blob/master/config.json)
- Sql file: [`schema.sql`](https://gitlab.com/rahul171/extra-data/mp-assignment/blob/master/schema.sql)
- npm package file: [`package.json`](https://gitlab.com/rahul171/extra-data/mp-assignment/blob/master/package.json)


### Multiple scripts explanation

I created two scripts, `index.js` and `index_2.js`.

`index_2.js` contains the simple solution, where i created different function for each resource query, e.g. `users` resource has a function which fetch users data from the api endpoint and insert that data into `rs_users` table, similarly `posts` resource has a function which fetch and store the data into `rs_post` table.

`index.js` uses a config object from `config.json` file and iterate though the resources array to fetch the resource and store them into the database. it doesn't have different function for different resource, it uses the same function and build query using the config object for each resource.

I think more suitable solution would be to create a script that takes json data and create tables and insert data into those tables according to the json object's structure.
A script that does the following:
```node
foreach (url of resourceUrls) {
  
  - create table for that resource. (e.g. users, posts, etc)
  - get resource data into dataArray.
  
  // e.g. this function will create 'geo', 'address' and 'company' tables
  // for the 'users' resource.
  createForeignTables(dataArray[0]);
  
  foreach (object of dataArray) {
    insertData(object);
  }
}

function createForeignTables(object) {
  foreach (property of object) {
    if (property is another object) {
      createForeign(object.property);
      - create table object.propertyName  
    }
  }
}

function insertData(object) {
  foreach (property of object) {
    if (property is another object) {
      insertData(object.property);    
    }
  }
  
  - insert object into objectName table
}
```

`createForeignTables` function will create tables for the objects inside the given object. eg. 'user' object contains 'address', 'company' which are also objects, 'address' contains 'geo' which is an object, so this function will create a 'geo' table first, then 'address' table and then 'company' table, thus creating the dependency tables for the 'users' table.

`insertData` function is same as `createForeignTables` function, but this function inserts data into corresponding tables.
