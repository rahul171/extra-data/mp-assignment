const fs = require('fs');
const axios = require('axios');
const pgp = require('pg-promise')();

const config = getConfig();
const db = pgp(config.pgUri);

(async () => {

  // Create tables by executing query file.
  await prepareDatabase(config.queryFile);

  // Fetch and store resources data into the database.
  await processResources(config.resources);

  // Close connection.
  db.$pool.end();
})();

/**
 * Prepare the database for resource data.
 *
 * @param queryFile Path to the query file.
 */
async function prepareDatabase(queryFile) {
  const file = getQueryFile(queryFile);
  await db.any(file);
}

/**
 * Process resource by fetching ans storing it into database.
 *
 * @param resources Resources configuration
 */
async function processResources(resources) {
  for (const resource of resources) {
    const data = await fetchData(resource.url);
    await insertData(resource.configs, data);
  }
}

/**
 * Get configuration from config.json file.
 *
 * @returns {Object} Configuration object.
 */
function getConfig() {
  const jsondata = fs.readFileSync('config.json');
  return JSON.parse(jsondata);
}

/**
 * Get a query file.
 *
 * @param path Path to the query file.
 * @returns {pgPromise.QueryFile} A query file object.
 */
function getQueryFile(path) {
  return new pgp.QueryFile(path, { minify: true });
}

/**
 * Fetch resource data from the URL.
 *
 * @param url Resource URL.
 * @returns {Array} An array of objects containing resource data.
 */
async function fetchData(url) {
  const res = await axios.get(url);
  return res.data;
}

/**
 * Insert resource data into database.
 *
 * @param configs A configuration array of a resource.
 * @param data Resource data.
 */
async function insertData(configs, data) {
  for (const config of configs) {

    // Build a query using database configuration and resource data.
    const query = buildQuery(config, data);

    // Execute the query.
    const res = await db.any(query);

    // Foreign ID property to be added into database in the next iterations.
    createForeignProperty(config, data, res);
  }
}

/**
 * Add foreign ID property to be added into database to the resource data object.
 *
 * @param config A resource configuration.
 * @param data Resource data.
 * @param res IDs of inserted rows of reference table
 */
function createForeignProperty(config, data, res) {
  if (!config.foreignName) {
    return false;
  }

  res.forEach((item, index) => {
    if (config.parent) {

      // eg. user.address.geoId
      data[index][config.parent][config.foreignName] = item.id;
    } else {

      // eg. user.companyId
      data[index][config.foreignName] = item.id;
    }
  });
}

/**
 * Build a query using resource configuration and resource data.
 *
 * @param config Resource configuration.
 * @param data  Resource data.
 * @returns {string} A query.
 */
function buildQuery(config, data) {

  // Get a table name from the config object.
  const table = getTableName(config);

  let query = `INSERT INTO ${table} `;

  query += columnsString(config.columns);
  query += ' VALUES ';
  query += valuesString(config, data);

  if (config.foreignName) {

    // Modify query to return id of the inserted row.
    query = addId(query);
  }

  return query;
}

function getTableName(config) {
  return config.tablePrefix + config.table;
}

/**
 * Build column part of a query.
 *
 * @param columns An array of columns.
 * @returns {string} Column part of the query.
 */
function columnsString(columns) {
  let s = '(';

  columns.forEach((item) => {
    s += `${item},`;
  });

  s = removeLastChar(s);
  s += ')';

  return s;
}

/**
 * Build values part of a query.
 *
 * @param config Resource configuration.
 * @param data Resource data.
 * @returns {string} Values part of the query.
 */
function valuesString(config, data) {
  let s = '';

  for (const item of data) {
    s += '(';

    for (const column of config.columns) {

      // eg. user.address
      let temp = item[config.table];

      if (config.parent) {

        // eg. user.address.geo
        temp = item[config.parent][config.table];
      }

      if (config.root) {

        // eg. user
        temp = item;
      }

      s += `'${temp[column]}',`;
    }

    s = removeLastChar(s);
    s += '),';
  }

  s = removeLastChar(s);

  return s;
}

/**
 * Remove last character from a string.
 *
 * @param s String
 * @returns {string} Modified string.
 */
function removeLastChar(s) {
  return s.slice(0, -1);
}

/**
 * Modify a query to return the id of inserted row.
 *
 * @param query The query.
 * @returns {string} Modified query.
 */
function addId(query) {
  return query + ' RETURNING id';
}
